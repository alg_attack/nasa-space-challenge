#!/usr/bin/env python
import json
from datetime import date, timedelta

import config
import ee

from flask import Flask, abort, request
import requests

GOOGLE_API_KEY = config.GOOGLE_API_KEY
HTTP_200_OK = 200

BANDS = ['B3', 'B4', 'B5', 'B6']

app = Flask(__name__)


def get_latitude_and_longitude_based_on_name(name):
    """
    Function to get latitude and longitude from Google maps based on a name of the place
    :param name: Name of a place
    :return: latitude and longitude
    """
    url = f'https://maps.googleapis.com/maps/api/geocode/json?address={name}&key={GOOGLE_API_KEY}'
    response = requests.get(url)
    if response.status_code == HTTP_200_OK:
        data = response.json()
        latitude = data['results'][0]['geometry']['location']['lat']
        longitude = data['results'][0]['geometry']['location']['lng']
        return (latitude, longitude)


def get_webhook_name():
    data = json.loads(request.data)
    if 'queryResult' in data:
        if 'location' in data['queryResult']['parameters']:
            return data['queryResult']['parameters']['location']['business-name']


def get_latitude_and_longitude(request):
    if request.args:
        if 'latitude' in request.args and 'longitude' in request.args:
            return request.args['latitude'], request.args['longitude']
        elif 'name' in request.args:
            return get_latitude_and_longitude_based_on_name(request.args['name'])
    return get_latitude_and_longitude_based_on_name(get_webhook_name())


def webhook_resposne():
    name = get_webhook_name()
    score = get_score()[-1]

    response = {
        "payload": {
            "google": {
                "expectUserResponse": True,
                "richResponse": {
                    "items": [
                        {
                            "simpleResponse": {
                                "textToSpeech": f"The {name} is {score}% safe"
                            }
                        }
                    ]
                }
            }
        }
    }
    return response


def get_score():
    ee.Initialize(config.EE_CREDENTIALS)

    latitudes = get_latitude_and_longitude(request)
    if latitudes:
        latitude, longitude = latitudes
    else:
        return abort(400)

    start_date = str(date.today() - timedelta(days=7))
    end_date = str(date.today())
    coordinates = ee.Geometry.Point(latitudes)
    collection = ee.ImageCollection(
        'COPERNICUS/S2_SR'
    ).filterDate(start=start_date, opt_end=end_date).filterBounds(coordinates).select(BANDS)
    values = ee.List(collection.getRegion(geometry=coordinates, scale=10)).getInfo()
    return values[-1]


@app.route('/', methods=['GET', 'POST'])
def alga():
    return str(get_score()[-1])


@app.route('/webhook', methods=['GET', 'POST'])
def google_assistance():
    return webhook_resposne()


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
